/*
    PointControll constructor
 */
function PointControll(config) {
    this.init(config);
}
PointControll.prototype.transfersLoopHandler = function() {
    var trainsInCollision = [],
        trainsInCollisionSorted,
        train;

    this.iterations++;
    for(var i = 0; i < this.trains.length; i++) {
        train = this.trains[i];
        train.stop();


        if (train.isNextStationCollisionPoint()) {
            trainsInCollision.push(train);
        } else {
            train.go();
        }
    }

    if (trainsInCollision.length > 0) {
        trainsInCollisionSorted = trainsInCollision.sort(function(trainA, trainB) {
            if (trainA.priority > trainB.priority) {
                return -1;
            }
            if (trainA.priority < trainB.priority) {
                return 1;
            }

            return 0;
        });

        for (var i = 0; i < trainsInCollision.length; i++) {
            train = trainsInCollisionSorted[i];
            if (i == 0) {
                train.go();
            } else {
                train.stop();
            }
        }
    }

    this.view.renderFront(this.trains);
};
PointControll.prototype.addTrain = function(config) {
    var train = new Train(config);
    this.trains.push(train);
};
PointControll.prototype.init = function(config) {
    var config = config || {},
        speed = config.speed || .05,
        distanceBetweenStations = config.distanceBetweenStations || 100;

    this.trains = config.trains;
    this.view = config.view;
    this.iterations = 0;
    this.frequency = Math.ceil(distanceBetweenStations / speed);
};
PointControll.prototype.run = function() {
    this.view.renderBack(this.trains);
    setInterval(this.transfersLoopHandler.bind(this), this.frequency);
    this.transfersLoopHandler();
};


/*
    Train constructor
 */
function Train(config) {
    for(var key in config) {
        this[key] = config[key];
    }

    this.priority = this.people || 0;
    this.currentStation = this.currentStation || 0;
    this.direction = this.direction || 1;
    this.status = 'stop';
}
Train.prototype.go = function() {
    this.status = 'go';
};
Train.prototype.stop = function() {
    if (this.status == 'go') {
        this.setNextStation();
    }
    this.status = 'stop';
};
Train.prototype.isNextStationCollisionPoint = function() {
    var direction = this.direction,
        nextStation = this.currentStation;
    if (this.currentStation == 0 && this.direction == -1 ||
        this.currentStation == this.stations - 1 && this.direction == 1) {
        direction *= -1;
    }

    nextStation += (direction == 1 ? 1 : -1);

    return nextStation == this.crossPointStation;
};
Train.prototype.setNextStation = function() {
    var stationPass = this.direction == 1 ? 1 : -1;
    this.currentStation += stationPass;

    if (this.currentStation > this.stations - 1 || this.currentStation < 0) {
        this.direction *= -1;
        stationPass = this.direction == 1 ? 1 : -1;
        this.currentStation += stationPass * 2;
    }
};
Train.prototype.log = function() {
    var logMessage = '';


    logMessage += 'status: ' + this['status'];


    var elem = document.getElementById(this.name);
    elem.innerHTML += '<div>' + logMessage + '</div>';
};



/*
    SubWay application module
 */
function SubWay(config) {
    this.init(config);
}
SubWay.prototype.init = function(config) {
    var view = new SubWayView({
            element: config.element,
            width: config.width,
            height: config.height
        }),
        trains = (function(trainsData) {
            var trains = [];
            for (var i = 0; i < trainsData.length; i++) {
                trains.push(new Train(trainsData[i]));
            }

            return trains;
        })(config.trains);


    this.pointControll = new PointControll({
        trains: trains,
        view: view
    });
};
SubWay.prototype.run = function() {
    this.pointControll.run();
};


/*
    SubWayView - view of subway
 */
function SubWayView(config) {
    if (typeof config.element == 'string') {
        this.container = document.querySelector(config.element);
    } else {
        this.container = config.element;
    }

    this.width = config.width || 300;
    this.height = config.height || 200;

    this.initCanvas();
}
SubWayView.prototype.initCanvas = function() {
    this.wrapper = document.createElement('div');
    this.wrapper.className = 'canvas-wrapper';
    this.wrapper.style.cssText = 'width: ' + this.width + 'px; height: ' + this.height + 'px;';

    this.canvasBackElement = document.createElement('canvas');
    this.canvasBackElement.className = 'canvas-back';
    this.canvasBackElement.setAttribute('width', this.width);
    this.canvasBackElement.setAttribute('height', this.height);

    this.canvasFrontElement = document.createElement('canvas');
    this.canvasFrontElement.className = 'canvas-front';
    this.canvasFrontElement.setAttribute('width', this.width);
    this.canvasFrontElement.setAttribute('height', this.height);

    this.wrapper.appendChild(this.canvasBackElement);
    this.wrapper.appendChild(this.canvasFrontElement);

    this.container.appendChild(this.wrapper);


    this.canvasBackCtx = this.canvasBackElement.getContext('2d');
    this.canvasFrontCtx = this.canvasFrontElement.getContext('2d');

    this.stationWidth = 50;
    this.stationRadius = 10;
};
SubWayView.prototype.renderBack = function(data) {
    this.renderLines(data);
};
SubWayView.prototype.renderLines = function(lines) {
    var angle = Math.round(180 / lines.length);

    for (var i = 0; i < lines.length; i++) {
        lines[i].angle = angle * i;
        this.renderLine(lines[i]);
    }

    this.renderCrossPoint();
};

SubWayView.prototype.renderCrossPoint = function() {
    this.canvasBackCtx.save();
    this.canvasBackCtx.translate(this.width / 2, this.height/2);
    this.canvasBackCtx.scale(1, -1);

    this.canvasBackCtx.fillStyle = 'pink';


    this.canvasBackCtx.beginPath();
    this.canvasBackCtx.moveTo(0, 0);
    this.canvasBackCtx.arc(0, 0, this.stationRadius, 0, 2 * Math.PI, false);
    this.canvasBackCtx.fill();
    this.canvasBackCtx.closePath();

    this.canvasBackCtx.restore();
};

SubWayView.prototype.renderLine = function(line) {
    this.canvasBackCtx.save();
    this.canvasBackCtx.translate(this.width / 2, this.height/2);
    this.canvasBackCtx.scale(1, -1);

    this.canvasBackCtx.rotate(this.getRadian(line.angle));

    var lineWidthBeforeCrossPoint = line.crossPointStation * this.stationWidth;
    this.canvasBackCtx.translate(-lineWidthBeforeCrossPoint, 0);

    this.canvasBackCtx.lineWidth = 5;
    this.canvasBackCtx.strokeStyle = line.colorLine || 'black';
    this.canvasBackCtx.fillStyle = line.colorLine || 'black';

    this.canvasBackCtx.beginPath();
    this.canvasBackCtx.moveTo(0, 0);
    this.canvasBackCtx.lineTo((line.stations - 1) * this.stationWidth, 0);
    this.canvasBackCtx.stroke();
    this.canvasBackCtx.closePath();

    this.canvasBackCtx.beginPath();
    this.canvasBackCtx.moveTo(0, 0);
    for (var i = 0; i < line.stations; i++) {
        if (i == line.crossPointStation) {
            continue;
        }
        this.canvasBackCtx.arc(i * this.stationWidth, 0, this.stationRadius, 0, 2 * Math.PI, false);

        if (i == 0) {
            this.renderLineText(i * this.stationWidth, 0, line.name, line.angle);
        }
    }
    this.canvasBackCtx.fill();
    this.canvasBackCtx.closePath();

    this.canvasBackCtx.restore();

};

SubWayView.prototype.renderLineText = function(x, y, name, angle) {
    this.canvasBackCtx.save();
    this.canvasBackCtx.scale(1, -1);
    this.canvasBackCtx.rotate(this.getRadian(angle));
    this.canvasBackCtx.font = '20pt Arial';
    this.canvasBackCtx.fillText(name, x - 28, y + this.stationRadius);
    this.canvasBackCtx.restore();
};

SubWayView.prototype.getRadian = function(grad) {
    return (Math.PI / 180) * grad;
};

SubWayView.prototype.renderFront = function(data) {
    this.clearFrontCanvas();
    this.renderTrains(data);
};
SubWayView.prototype.clearFrontCanvas = function() {
    this.canvasFrontCtx.clearRect(0, 0, this.width, this.height);
};
SubWayView.prototype.renderTrains = function(trains) {
    var angle = Math.round(180 / trains.length);
    for(var i = 0; i < trains.length; i++) {
        trains[i].angle = angle * i;
        this.renderTrain(trains[i]);
    }
};
SubWayView.prototype.renderTrain = function(train) {
    var trainCicleSize = 5;
    this.canvasFrontCtx.save();
    this.canvasFrontCtx.translate(this.width / 2, this.height/2);
    this.canvasFrontCtx.scale(1, -1);

    this.canvasFrontCtx.rotate(this.getRadian(train.angle));

    var distanceToCrossPoint = train.crossPointStation * this.stationWidth;
    this.canvasFrontCtx.translate(-distanceToCrossPoint, 0);


    this.canvasFrontCtx.fillStyle = train.color || 'yellow';
    this.canvasFrontCtx.beginPath();
    this.canvasFrontCtx.moveTo(0, 0);
    this.canvasFrontCtx.arc(train.currentStation * this.stationWidth, 0, trainCicleSize, 0, 2 * Math.PI, false);
    this.canvasFrontCtx.fill();
    this.canvasFrontCtx.closePath();


    this.canvasFrontCtx.restore();
};




/*
    running code
 */

document.addEventListener('DOMContentLoaded', function() {
    var subWay = new SubWay({
        element: '#container',
        trains: [
            {
                name: 'b',
                people: 6,
                currentStation: 0,
                crossPointStation: 2,
                stations: 5,
                direction: 1,
                color: 'yellow',
                colorLine: 'blue'
            },
            {
                name: 'c',
                people: 2,
                currentStation: 0,
                crossPointStation: 1,
                stations: 4,
                direction: 1,
                color: 'white',
                colorLine: 'green'
            },
            {
                name: 'a',
                people: 8,
                currentStation: 0,
                crossPointStation: 1,
                stations: 3,
                direction: 1,
                color: 'brown',
                colorLine: 'red'
            }
        ]
    });
    subWay.run();
});

